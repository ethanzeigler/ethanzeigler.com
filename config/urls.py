from django.conf import settings
from django.contrib import admin
from django.urls import include, path

from datascience.apps import DatascienceConfig

from users.models import CustomUser


admin.site.site_header = "Control Panel"
admin.site.site_title = 'EZ Control Panel'

urlpatterns = [
    path("admin/", admin.site.urls),
    path("accounts/", include("allauth.urls")),
    path("datascience/", include("datascience.urls", namespace="datascience")),
    path("", include("pages.urls")),
]

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        path("__debug__/", include(debug_toolbar.urls)),
        path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
    ] + urlpatterns
