# ethanzeigler.com development notes

## Current tasks

**Users can:**
- [ ] edit account details
- [ ] only submit when permitted
- [ ] view their submissions
- [ ] delete their submissions

**Users can't:**
- [ ] log in when banned

**Staff can:**
- [ ] ban users


## Completed tasks

#### 3/5/21
**Users can:**
- [x] sign up
- [x] sign in
- [x] sign out

**Users will:**
- [x] get 'User' group on signup
- [x] be contacted to confirm email

**Users see:**
- [x] (UGLY) sign in / account button in navbar

----
#### ?
**Anyone can:**
- CRUD w101 datascience objects
- moderate
