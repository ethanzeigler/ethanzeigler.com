// ds_droprate.js
// datascience droprate applet 
const { DJAjaxFormModal } = require("../global_lib/formUtil");

document.addEventListener('DOMContentLoaded', () => {
    // // Category submittal
    // registerDJAjaxFormModal({
    //     modal_id: 'idCategoryForm',
    //     form_id: 'w101CategoryForm',
    //     submit_button_id: "submitCategoryFormBtn",
    //     submit_to: "/datascience/w101_droprate/category",
    //     success_text: "Created new category. It won't be visible to other wizards until approved.",
    //     error_text: "Something went wrong creating the new category"
    // });

    // Category submittal
    new DJAjaxFormModal({
        modal_id: 'idCategoryForm',
        form_id: 'w101CategoryForm',
        submit_button_id: "submitCategoryFormBtn",
        submit_to: "/datascience/w101_droprate/category",
        success_text: "Created new category. It won't be visible to other wizards until approved.",
        error_text: "Something went wrong creating the new category"
    });

    // Item submittal
    new DJAjaxFormModal({
        modal_id: 'idItemForm',
        form_id: 'w101ItemForm',
        submit_button_id: "submitItemFormBtn",
        submit_to: "/datascience/w101_droprate/item",
        success_text: "Created new item. It won't be visible to other wizards until approved.",
        error_text: "Something went wrong creating the new item"
    });

    // Item submittal
    new DJAjaxFormModal({
        modal_id: 'idItemSourceForm',
        form_id: 'w101ItemSourceForm',
        submit_button_id: "submitItemSourceFormBtn",
        submit_to: "/datascience/w101_droprate/item_source",
        success_text: "Created new item source. It won't be visible to other wizards until approved.",
        error_text: "Something went wrong creating the new item source"
    });

    // Attempt submittal
    new DJAjaxFormModal({
        modal_id: 'idDropAttemptForm',
        form_id: 'w101DropAttemptForm',
        submit_button_id: "submitDropAttemptFormBtn",
        submit_to: "/datascience/w101_droprate/drop_attempt/form",
        success_text: "Recorded. Thank you!",
        error_text: "Something went wrong creating the drop attempt",
        special_fields: {
            "source": {
                type: "autocomplete",
                typeArgs: {
                    Url: "/datascience/w101_droprate/item_source/autocomplete",
                    EmptyMessage: "Nothing found",
                    MinChars: 2,
                }
            },
            "w101dropcount_set-0-item": {
                type: "autocomplete",
                typeArgs: {
                    Url: "/datascience/w101_droprate/item/autocomplete",
                    EmptyMessage: "No item found",
                    MinChars: 2,
                }
            },
            "w101dropcount_set-1-item": {
                type: "autocomplete",
                typeArgs: {
                    Url: "/datascience/w101_droprate/item/autocomplete",
                    EmptyMessage: "No item found",
                    MinChars: 2,
                }
            },
            "w101dropcount_set-2-item": {
                type: "autocomplete",
                typeArgs: {
                    Url: "/datascience/w101_droprate/item/autocomplete",
                    EmptyMessage: "No item found",
                    MinChars: 2,
                }
            }
        }
    });
});
