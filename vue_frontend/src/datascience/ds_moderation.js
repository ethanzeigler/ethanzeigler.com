// ds_droprate.js
// datascience droprate applet
import { createApp } from 'vue';
import App from "./apps/ModerationApp.vue";
// import UniqueId from 'vue-unique-id';


document.addEventListener('DOMContentLoaded', () => {
    if (document.getElementById("moderation-app")) {
        const app = createApp(App);
        // app.use(UniqueId);
        app.mount("#moderation-app");
    }
});
