const AutoComplete = require("autocomplete-js");
const { showAlert } = require("./alerts");
const { getCookie } = require("./misc");
const axios = require("axios");
const bootstrap = require("bootstrap");
// var AutoComplete = require("autocomplete-js");

const autocomplete_defaults = {
    "_Error": function() {
        showAlert({
            message: `Error: autocomplete couldn't contact server`,
            level: "danger",
            dismissable: true,
            timeout: 10
        });
    },
    // copied and modified from the default handler
    // "this" refers to the autocomplete object
    "_Select": function (item) {
        if (item.hasAttribute("data-autocomplete-value")) {
            this.Input.dataset.submittedValue = item.getAttribute("data-autocomplete-value");
            this.Input.value = item.textContent;
        } else {
            this.Input.value = item.textContent;
        }
        // will cause bug with keyboard shortcuts
        // not really a concern right now
        this.Input.setAttribute("data-autocomplete-old-value", this.Input.value);
    },
    _Focus: function () {
        var oldValue = this.Input.getAttribute("data-autocomplete-old-value");

        if (this.Input.value !== oldValue && this._MinChars() <= this.Input.value.length) {
            this.DOMResults.setAttribute("class", "autocomplete open");
        }
    },
}

export function buildValidationText({
    text = null,
    clearOn = null,
    isInvalidText = null
}) {
    var bsFeedbackText = document.createElement("div")
    if (isInvalidText) {
        bsFeedbackText.classList.add("invalid-feedback")
    } else(
        bsFeedbackText.classList.add("valid-feedback")
    )
    bsFeedbackText.dataset.clearOn = clearOn;
    bsFeedbackText.innerText = text;
    return bsFeedbackText;
}


export class DJField {
    constructor({
        name,
        id = null,
        optional = false,
        special = null
    }) {
        this.name = name;
        this.id = id;
        this.optional = optional;
        this.special = special;
    }
}


export class DJAjaxFormModal {
    constructor({
        modal_id,
        form_id,
        special_fields = {},
        submit_button_id,
        submit_to,
        success_text = null,
        error_text = null
    }) {
        document.getElementById(form_id).reset();

        // special field registration
        Object.entries(special_fields).forEach(([name, data]) => {
            // autocomplete using autocomplete-js
            if (data.type === "autocomplete") {
                // give default values
                const typeArgs = Object.assign({}, autocomplete_defaults, data.typeArgs);
                // can only be registered when the modal opens or it gets confused
                // and the dropdown doesn't position right
                document.querySelector(`#${modal_id}`).addEventListener("shown.bs.modal", () => {
                        console.log(`Registered autocomplete ${name}`);
                        AutoComplete(typeArgs, `#${form_id} input[name="${name}"]`);
                    }, {
                        once: true
                    }
                );
            } else {
                console.log(`Unknown special category ${data.type}`);
            }
        });

        // form submittal
        document.getElementById(submit_button_id).addEventListener("click", () => {
            // not using standard form submittal, so grab values and add them to data
            var bodyFormData = new FormData();
            document.getElementById(form_id).querySelectorAll("input,textarea,select").forEach((e) => {
                if (e.hasAttribute("data-submitted-value")) {
                    bodyFormData.append(e.name, e.dataset.submittedValue)
                } else {
                    bodyFormData.append(e.name, e.value);
                }
            });

            // deletes error text and the like
            document.querySelectorAll(`[data-clear-on='${submit_button_id}']`).forEach((e) => e.remove());

            axios.post(submit_to, bodyFormData, {
                method: "POST",
                headers: {
                    'Content-Type': 'multipart/form-data',
                    'X-CSRFToken': getCookie("csrftoken"),
                    'X-IS-AJAX': true
                }
            }).then((response) => {
                console.log(response);
                showAlert({
                    level: "success",
                    message: success_text,
                    dismissable: true,
                    timeout: 8
                });
                // ensure we close and clear the modal
                bootstrap.Modal(`#${modal_id}`).hide();
                document.getElementById(form_id).reset();
            }).catch((response) => {
                // form was invalid / error occurred
                console.log(response);
                // add error reason to field
                if (response.response.status == 400) {
                    // something about our form was wrong
                    // add error text
                    document.getElementById(form_id).querySelectorAll("input,textarea,select").forEach((e) => {
                        const name = e.name;
                        if (name === undefined) {
                            return;
                        }
                        if (response.response.data[name]) {
                            e.setCustomValidity(response.response.data[name][0]);
                            e.classList.add("is-invalid");
                            e.insertAdjacentElement(
                                "afterend",
                                buildValidationText({
                                    text: response.response.data[name][0],
                                    clearOn: submit_button_id,
                                    isInvalidText: true
                                })
                            );
                        }
                    });
                } else {
                    // internal error that's unrelated to the form
                    showAlert({
                        level: "warning",
                        message: `${error_text}. Error code: ${response.response.status}`,
                        dismissable: true,
                    });
                }
            })
        });

        // prevent normal form submittal
        document.getElementById(form_id).addEventListener("submit", (e) => e.preventDefault(), true);
    }
}
