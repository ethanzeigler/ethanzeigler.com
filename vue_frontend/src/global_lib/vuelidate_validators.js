import { helpers } from '@vuelidate/validators';
import { showAlert } from './alerts';

export function oneOf(param) {
    return (value) => param.includes(value)
}

// Validates whether or not an external validation record exists in the
// component. Error message is set to its content.
export function backendApproved(externalValidationName, component) {
    // Future me, this is full of a lot of weird hacks. Apologies in advance.
    return helpers.withParams(
        {
            externalName: externalValidationName,
            component: component
        },
        helpers.withMessage(
            ({ $params }) => {
                return `SERVER: ${($params.component.external_validation[$params.externalName] || ["NONE"])[0]}`
            },
            (_value, vm) => {
                const errs = vm.external_validation;
                showAlert({
                    message: `validating ${externalValidationName}: ${!(errs && (externalValidationName in errs))}`,
                    dismissable: true,
                    timeout: 3
                });
                // no errors or no errors for name
                return !(errs && (externalValidationName in errs));
            }
        )
    );
}