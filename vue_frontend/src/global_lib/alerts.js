export function showAlert({
    level = "info",
    message,
    dismissable = true,
    timeout = null
}) {
    var valid_category = [
        "primary",
        "secondary",
        "success",
        "danger",
        "warning",
        "info",
        "light",
        "dark"
    ].includes(level);

    console.debug("Alert: " + message);
    if (!valid_category) {
        console.warn("Alert: Invalid category name " + level);
        return;
    }

    var alert = document.createElement('div');
    alert.setAttribute('role', 'alert');
    // TODO: make x stick on top right for alerts
    alert.classList.add('shadow', 'alert', 'alert-' + level, 'fade', 'show', 'position-relative');

    if (dismissable) {
        alert.classList.add('alert-dismissable');
        alert.innerHTML = message + `<button type="button" class="btn-close pl-1" data-bs-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>`;
    } else {
        alert.innerHTML = message;
    }

    // alert-container sticks on the left top under navbar
    const alertContainer = document.getElementById("alert-container");
    if (!alertContainer) {
        console.error("Alert: No alert container found");
        return
    }

    alertContainer.prepend(alert);
    // fade in
    alert.classList.add('in');

    if (timeout) {
        setTimeout(function () {
            alert.remove();
        }, timeout * 1000);
    }
}