import { ref, computed, reactive } from "vue";

function createModel() {
  
}

export default function() {
  const createModel = function()
  const count = ref(0);
  const double = computed(() => count.value * 2)
  function increment() {
    count.value++;
  }
  return {
    count,
    double,
    increment
  }
}
