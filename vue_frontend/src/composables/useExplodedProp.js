import {
    ref,
    computed,
    watch,
    onBeforeMount,
    onMounted
} from "vue";

// explodes a object prop into a set of computed properties
// emits update to prop with update:propName
function useExplodedProp(props, context, propName, emitCondition = () => {
    true
}) {
    const exploded = {};
    Object.entries(props[propName]).forEach(([k, v]) => {
        exploded[k] = computed(() => {
            get: () => props[propName][k];
            set: (val) => {
                let copy = Object.assign({}, props[propName]);
                copy[k] = val;
                if (emitCondition) {
                    context.emit(
                        `update:${propName}`,
                        copy
                    );
                }
            }
        });
    });

    return exploded;
}