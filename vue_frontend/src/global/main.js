// core.js
// site-wide javascript
// loaded on all pages

// const { showAlert } = require("../global_lib/alerts");
const bootstrap = require("bootstrap");
import "../assets/sass/project.scss";

console.log("loaded");

document.addEventListener('DOMContentLoaded', () => {
    // Enable tooltips
    [].slice
    .call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
        .forEach(e => { new bootstrap.Tooltip(e) });
        
    // When toggle is already used
    [].slice
        .call(document.querySelectorAll('[data-bs-tooltip="tooltip"]'))
        .forEach(e => { new bootstrap.Tooltip(e) });
    
    // showAlert({
    //     message: "core loaded",
    //     timeout: 3
    // });
});
