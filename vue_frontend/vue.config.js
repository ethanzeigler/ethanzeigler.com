
const developmentPort = 8001;

const pages = {
    'global': {
        entry: './src/global/main.js',
        chunks: ['chunk-vendors']
    },
    'datascience/moderation': {
        entry: './src/datascience/ds_moderation.js',
        chunks: ['chunk-vendors']
    },
    'datascience/w101_droprate': {
        entry: './src/datascience/ds_droprate.js',
        chunks: ['chunk-vendors']
    },
    'vue_landing_page': {
        entry: './src/main.js',
        chunks: ['chunk-vendors']
    },
    // 'vue_app_02': {
    //     entry: './src/newhampshir.js',
    //     chunks: ['chunk-vendors']
    // },
}

function publicPath() {
    let ip = "localhost";
    if (process.env.IP != undefined) {
        ip = process.env.IP;
        console.log(`IP overridden by environment [${ip}]`)
    }
    let pp = process.env.NODE_ENV === "development" ? `http://${ip}:${developmentPort}/` : '/static/';
    if (process.env.PUBLIC_PATH_OVERRIDE != undefined) {
        pp = process.env.PUBLIC_PATH_OVERRIDE;
        console.log(`Public path overridden by environment [${pp}]`)
    }
    console.log(`Public path evaluated to [${pp}]`)
    return pp;
}


const BundleTracker = require("webpack-bundle-tracker");
module.exports = {
    pages: pages,
    filenameHashing: false,
    productionSourceMap: false,
    publicPath: publicPath(),
    outputDir: '../vue_frontend/dist/',
    css: {
        extract: true,
        sourceMap: true
    },
    chainWebpack: config => {
        config.devtool('source-map');
        config.resolve.symlinks(false);

        // Split vendor code into separate package.
        config.optimization
            .splitChunks({
                cacheGroups: {
                    vendor: {
                        test: /[\\/]node_modules[\\/]/,
                        name: "chunk-vendors",
                        chunks: "all",
                        priority: 1
                    }
                },
            })
            .runtimeChunk("single");
        
        // Add dependency flags to modules that aren't global
        // TODO?

        // Object.keys(pages).forEach(page => {
        //     config.plugins.delete(`html-${page}`);
        //     config.plugins.delete(`preload-${page}`);
        //     config.plugins.delete(`prefetch-${page}`);
        // })

        // // this breaks stuff for some reason
        // config.plugins.delete('hmr');

        // Without this, the relative imports for images don't seem to resolve
        // in the sass. It's supposed to enable url resolution,
        // but that doesn't seem to work. However if I get rid of it,
        // relative doesn't work. Might just not be understanding
        // what it does. Oh well. It makes stuff work. Also
        // complains about not getting a sourcemap but can't please everyone.
        ['vue-modules', 'vue', 'normal-modules', 'normal'].forEach(rule => {
			config.module.rule('scss')
				.oneOf(rule)
				.use('resolve-url-loader')
				.loader('resolve-url-loader')
				.before('sass-loader')
				.end()
				.use('sass-loader')
				.loader('sass-loader')
				.tap(options =>
					({...options, sourceMap: true})
				);
		});

        // Tells django what bundles exist and where.
        // Means that django doesn't care about the sourcing of the files.
        // Great for dev work where it's handled live.
        config
            .plugin('BundleTracker')
            .use(BundleTracker, [{filename: './dist/webpack-stats.json'}]);

        config.resolve.alias
            .set('__STATIC__', 'static')

        config.devServer
            .public(`http://0.0.0.0:${developmentPort}`)
            .host('localhost')
            .port(developmentPort)
            .hotOnly(true)
            .watchOptions({poll: 1000})
            .https(false)
            .headers({"Access-Control-Allow-Origin": ["*"]})
    }
};