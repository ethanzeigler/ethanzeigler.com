"""
Create permission groups
Create permissions (read only) to models for a set of groups
"""
import logging

from django.contrib.auth.models import Group, Permission
from django.core.management.base import BaseCommand
from django_extensions.management.shells import import_objects

from datascience.models import W101Category
from users.models import CustomUser

# NOTE: https://stackoverflow.com/questions/48544176/how-to-set-default-group-for-new-user-in-django/48544585
# https://stackoverflow.com/questions/2297377/how-do-i-prevent-permission-escalation-in-django-admin-when-granting-user-chang

# NOTE: django naming scheme is add, change, delete, view
# My custom ones are
#   access: use app,
#   _my: _ models created by self,
#   _myprotected: _ models created by self that have been protected
#                 In cases where this is django enforced, idk yet
GROUPS = [
    {
        "name": "Admin",
        "permissions": [],
        "default": False,
    },
    {
        "name": "Staff",
        "permissions": [
            # TODO: block changing of permissions
            "users.view_customuser",
        ],
        "default": False,
    },
    {
        "name": "Data Moderator",
        "permissions": [
            "datascience.change_w101category",
            "datascience.change_w101dropattempt",
            "datascience.change_w101itemsource",
            "datascience.change_w101item",
            "datascience.change_w101dropcount",
            "datascience.access_moderationqueue",
            "datascience.access_modmail",
        ],
        "default": False,
    },
    {
        "name": "User",
        "permissions": [
            "datascience.add_w101category",
            "datascience.add_w101dropattempt",
            "datascience.add_w101itemsource",
            "datascience.add_w101item",
            "datascience.add_w101dropcount",
            "datascience.access_dataentry",
        ],
        "default": True,
    },
]

DEFAULT_SUPERUSER = {
    "username": "superuser",
    "email": "superuser@example.com"
}

CUSTOM_PERMISSIONS = {
    "datascience.access_modmail": {
        "description": "Can access moderator mail",
    },
    "datascience.access_moderationqueue": "Can access moderation queue",
    "datascience.access_dataentry": "Can access data entry",
}



def createDSObject():
    ...


class Command(BaseCommand):
    help = "Initializes the database for the datascience application"

    def handle(self, *args, **options):
        # SUPERUSER ACCOUNT
        su = CustomUser.objects.filter(username=DEFAULT_SUPERUSER["username"]).first()
        if su is None and input("Create superuser? [y/N]: ") == "y":
            import getpass
            passwd = getpass.getpass("Password: ")
            CustomUser.objects.create_superuser(username=DEFAULT_SUPERUSER["username"], password=passwd, email=DEFAULT_SUPERUSER["email"])
            print("Superuser created. Continuing...")
        
        # PERMISSIONS AND GROUPS
        for group in GROUPS:
            new_group, created = Group.objects.get_or_create(name=group["name"])
            if created:
                print(f"Created group {group['name']}")
            else:
                print(f"Group '{group['name']} already exists'")
            for permission_id in group["permissions"]:
                app_label = permission_id.split(".")[0]
                codename = permission_id.split(".")[1]

                permission = Permission.objects.filter(codename=codename, content_type__app_label=app_label).first()
                if permission is None:
                    print(f"    ? Permission '{permission_id}' does not exist. Cannot add to group.") 
                elif new_group.permissions.filter(codename=codename).count() == 0:
                    print(f"    + Adding permission {permission_id}")
                    new_group.permissions.add(permission)
                else:
                    print(f"    = Permission '{permission_id}' is already included")
        print("=" * 10)
        print("Created default group and permissions.")