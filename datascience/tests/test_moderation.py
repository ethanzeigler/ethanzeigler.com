from django.test import TestCase, Client
from users.models import CustomUser
from datascience.models import W101Category, W101Item, WorkflowState


class ModerationAPITest(TestCase):
    def setUp(self) -> None:
        self.test_moderator = CustomUser.objects.create(username="test_moderator")
        self.test_moderator.set_password("test_password")
        self.test_moderator.save()

        self.moderated_category = W101Category.objects.create(
            created_by=self.test_moderator,
            name="Test Category",
            description="Test Description",
            applies_to=W101Category.APPLIES_TO_ANY,
        )

        self.moderated_item = W101Item.objects.create(
            created_by=self.test_moderator,
            name="Test Category",
            description="Test Description",
            category=self.moderated_category
        )

    def approve_item_correctly(self, moderated, client):
        resp = client.post(
            "/datascience/moderation/",
            '{"action": "approve", "uuid": "%s"}' % moderated.external_id,
            content_type="application/json"
        )
        self.assertEqual(resp.status_code, 200)
        moderated.refresh_from_db()
        self.assertEqual(moderated.workflow_state, WorkflowState.APPROVED)
        self.assertEqual(moderated.approved_by, self.test_moderator)


    def test_acceptance_happy_path(self):
        client = Client()
        client.login(username="test_moderator", password="test_password")
        self.approve_item_correctly(self.moderated_category, client)
        self.approve_item_correctly(self.moderated_item, client)


    def test_acceptance_unauthenticated(self):
        client = Client()
        self.approve_item_correctly(self.moderated_category, client)
        self.approve_item_correctly(self.moderated_item, client)

