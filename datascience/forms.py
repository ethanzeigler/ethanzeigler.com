from crispy_forms.helper import FormHelper
from crispy_forms.layout import Button, Div, Field, Fieldset, HTML, Layout, Submit
from django.db.models.fields import CharField
#from dal import autocomplete, forward
from django.forms import formset_factory
from django import urls
import django.forms.models as models
import django.forms.widgets as widgets
from django.urls.base import reverse
from django.utils.html import urlize
from pages.crispy_layouts import Formset

import datascience.models as ds_models
from datascience.models import (
    W101Category,
    W101DropAttempt,
    W101DropCount,
    W101Item,
    W101ItemSource,
)

class ModelAutocompleteField(models.ModelChoiceField):
    ...

class W101CategoryForm(models.ModelForm):
    """Category creation form intended to be within a bootstrap4 modal

    Args:
        ModelForm ([type]): [description]

    Returns:
        [type]: [description]
    """

    class Meta:
        model = W101Category
        fields = ["name", "applies_to", "description"]
        label = "Category"
        name = "CategoryForm"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_id = "w101CategoryForm"
        self.helper.form_method = "post"


class W101ItemForm(models.ModelForm):
    """Item creation form intended to be within a bootstrap4 modal

    Args:
        ModelForm ([type]): [description]

    Returns:
        [type]: [description]
    """

    class Meta:
        model = W101Item
        fields = ["name", "category", "description"]
        label = "Item"
        name = "ItemForm"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_id = "w101ItemForm"
        self.helper.form_method = "post"


class W101ItemSourceForm(models.ModelForm):
    """Item source creation form intended to be within a bootstrap4 modal

    Args:
        ModelForm ([type]): [description]

    Returns:
        [type]: [description]
    """

    class Meta:
        model = W101ItemSource
        fields = ["name", "category", "description"]
        label = "Item Source"
        name = "ItemSourceForm"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_id = "w101ItemSourceForm"
        self.helper.form_method = "post"


class W101DropCountForm(models.ModelForm):
    class Meta:
        model = W101DropCount
        fields = ["item", "count"]
        widgets = {
            "item": widgets.TextInput({"placeholder": "Start typing..."})
        }
        labels = {"count": "#"}
        label = "ItemSource"
        name = "ItemSourceForm"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_id = "w101DropCountForm"
        self.helper.form_method = "post"
        self.helper.layout = Layout(
            Div(
                Field("count", wrapper_class="mr-3", style="width: 5rem"),
                Field("item"),
                css_class="d-flex",
            )
        )


W101DropCountFormset = models.inlineformset_factory(
    parent_model=W101DropAttempt,
    model=W101DropCount,
    fields=["item", "count"],
    labels={"count": "#"},
    extra=3,
    max_num=10,
    min_num=1,
    form=W101DropCountForm,
)


class W101DropAttemptForm(models.ModelForm):
    class Meta:
        model = W101DropAttempt
        fields = ["source", "opportunities_count"]
        labels = {"count": "#"}
        widgets = {
            "source": widgets.TextInput({"placeholder": "Start typing...", })
        }
        label = "Drop Attempt"
        name = "DropAttemptForm"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_id = "w101DropAttemptForm"
        self.helper.form_method = "post"
        self.helper.layout = Layout(
            Field("source"),
            Field("opportunities_count"),
            HTML("<b>Drops</b>"),
            Formset(
                # loads from the view context
                "drop_count_formset",
            ),
            # Button("", "Add another", data_),
        )

    def save(self, commit=True):
        return super(W101DropAttemptForm, self).save(commit=commit)


# class W101CritDPForm(ModelForm):
#     """
#     Form for W101CritDP
#     """

#     def __init__(self, *args, **kwargs):
#         super().__init__(*args, **kwargs)
#         self.helper = FormHelper()
#         self.helper.form_id = "w101_crit_form"
#         self.helper.form_method = "post"
#         self.helper.form_action = reverse(
#             "datascience:new_datapoint", args=("w101crit",)
#         )
#         self.helper.add_input(Submit("submit", "Submit"))
#         self.helper.layout = Layout(
#             Field(
#                 "level",
#                 css_class="field-short no-spinner",
#                 max=130,
#                 min=1,
#                 placeholder=1,
#             ),
#             Field(
#                 "rating",
#                 css_class="field-short no-spinner",
#                 max=1000,
#                 min=0,
#                 placeholder=0,
#             ),
#             Field(
#                 "percent",
#                 css_class="field-short no-spinner",
#                 max=100,
#                 min=0,
#                 placeholder=0,
#             ),
#         )

#     def form_valid(self, form):
#         self.object = form.save(commit=False)
#         # do something with self.object
#         from django.http import HttpResponseRedirect

#         return HttpResponseRedirect(self.get_success_url())

#     class Meta:
#         fields = ["level", "rating", "percent"]
