from datascience.util.json_validator import JsonValidator
import json
import logging
from typing import Tuple


from django import template
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.http import HttpRequest
from django.http.response import (
    HttpResponse,
    JsonResponse,
)
from django.shortcuts import render
from django.views.generic.base import TemplateView, View

from datascience import projects
from datascience.forms import (
    W101CategoryForm,
    W101DropAttemptForm,
    W101DropCountFormset,
    W101ItemForm,
    W101ItemSourceForm,
)
from datascience.models import ModeratedModel, WorkflowState

###########################################################
# Instance
logger = logging.getLogger(__name__)


###########################################################
# Application Views
class IndexView(TemplateView):
    """
    Project list page. Displays active projects and their
    subtitle description.
    """

    template_name = "datascience/index.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["projects"] = projects.projects
        return context


class W101DropRateDashboardView(TemplateView):
    template_name = "datascience/w101_droprate/dashboard.html"
    project_slug = None

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["category_form"] = W101CategoryForm()
        context["item_form"] = W101ItemForm()
        context["item_source_form"] = W101ItemSourceForm()
        context["drop_attempt_form"] = W101DropAttemptForm()
        context["drop_count_formset"] = W101DropCountFormset()

        return context


class W101DropAttemptEndpointView(View):
    def post(self, request):
        # Add authentication
        drop_form = W101DropAttemptForm(request.POST)
        drop_count_formset = W101DropCountFormset(request.POST)

        if not drop_form.is_valid():
            return JsonResponse(drop_form.errors, status=400)
        if not drop_count_formset.is_valid():
            return JsonResponse(drop_count_formset.errors, safe=False, status=400)

        df = drop_form.save()
        dcf = drop_count_formset.save(commit=False)
        for drop_count in dcf:
            drop_count.attempt = df
            drop_count.save()
        return HttpResponse("", status=201)


# @permission_required('')
# class W101CritDPCreate(CreateView, SuccessMessageMixin, LoginRequiredMixin):
#     template_name = "datascience/create.html"
#     success_url = reverse_lazy("datascience:dashboard", args=("w101crit",))
#     success_message = "Data recorded. Thank you!"
#     form_class = W101CritDPForm


###########################################################
# Template Filters
register = template.Library()


###########################################################
# API views


class ModerationView(PermissionRequiredMixin, View):
    permission_required = "polls.add_choice"

    APPROVAL_JSON_SCHEMA = {
        "$schema": "http://json-schema.org/draft-07/schema#",
        "type": "object",
        "properties": {
            "action": {"type": "string", "enum": ["approve", "deny"]},
            "uuid": {"type": "string"},
        },
        "required": ["action", "uuid"],
    }

    def is_detail_request(self, request: HttpRequest):
        requested_id = request.GET.get("id", None)
        return requested_id not in [None, "all"]

    def is_list_request(self, request: HttpRequest):
        return request.GET.get("id", None) == "all"

    def get_moderated_via_post(self, request) -> Tuple:
        try:
            # load and check the json formatting
            json_data = json.loads(request.body.decode("utf-8"))
            external_id = json_data.get("uuid")
            return (
                ModeratedModel.objects.filter(external_id=external_id)
                .select_subclasses()
                .first()
            )
        except json.JSONDecodeError:
            logger.debug(
                "Moderation call body couldn't be deciphered",
                extra={"body": request.body.decode("utf-8")},
            )
            return None

    def get(self, request):
        if self.is_detail_request(request):
            external_id = request.GET.get("id", None)
            return JsonResponse(
                {
                    "status": 200,
                    "moderated": [
                        ModeratedModel.objects.filter(
                            external_id=external_id, workflow_state=WorkflowState.NEW
                        )
                        .select_subclasses()
                        .first()
                        .to_moderation_data()
                    ],
                    "s": request.body.decode("utf-8"),
                }
            )
        elif self.is_list_request(request):
            return JsonResponse(
                {
                    "status": 200,
                    "moderated": [
                        x.to_moderation_data()
                        for x in ModeratedModel.objects.filter(
                            workflow_state=WorkflowState.NEW
                        )
                        .select_subclasses()
                        .all()
                    ],
                    "s": request.body.decode("utf-8"),
                }
            )
        else:
            return render(request, template_name="datascience/moderation/index.html")

    def post(self, request):
        validator = JsonValidator(
            self.APPROVAL_JSON_SCHEMA, request.body.decode("utf-8")
        )
        if not validator.valid:
            logger.debug(f"Invalid format for moderation request by {request.user}")
            return JsonResponse(
                {
                    "status": 400,
                    "result": f"invalid request: {validator.validation_message}",
                    "s": request.body.decode("utf-8"),
                },
                status=400,
            )

        moderated = self.get_moderated_via_post(request)
        if moderated is None:
            return JsonResponse(
                {
                    "status": 404,
                    "result": "no such id",
                    "s": request.body.decode("utf-8"),
                },
                status=404,
            )
        if validator.data["action"] == "deny":
            moderated.deny(user=request.user)
            # TODO: Send message to user about denial
            return JsonResponse({"status": 200, "result": "ok"})
        if moderated.approve(request.user):
            return JsonResponse({"status": 200, "result": "ok"})
        else:
            return JsonResponse(
                {"status": 422, "result": "failed to transition approval state"},
                status=422,
            )
