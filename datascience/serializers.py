from django.forms.fields import CharField
from rest_framework import serializers

from datascience.models import W101Category, W101Item, W101DropAttempt, W101ItemSource


class W101CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = W101Category
        fields = [
            "external_id",
            "created_at",
            "created_by",
            "updated_at",
            "workflow_state",
            "approved_by",
            "name",
            "applies_to",
            "description",
        ]


class W101ItemSourceSerializer(serializers.ModelSerializer):
    class Meta:
        model = W101ItemSource
        fields = [
            "external_id",
            "created_at",
            "created_by",
            "updated_at",
            "workflow_state",
            "approved_by",
            "name",
            "description",
            "category",
        ]


class W101ItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = W101Item
        fields = [
            "external_id",
            "created_at",
            "created_by",
            "updated_at",
            "workflow_state",
            "approved_by",
            "name",
            "description",
            "category",
        ]


class W101DropAttemptSerializer(serializers.ModelSerializer):
    drops = W101ItemSerializer(read_only=True, many=True)

    class Meta:
        model = W101DropAttempt
        fields = [
            "external_id",
            "created_at",
            "updated_at",
            "created_by",
            "opportunities_count",
            "source",
            "drops",
        ]
