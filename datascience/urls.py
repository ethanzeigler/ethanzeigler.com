from django.urls import path

from datascience.apps import DatascienceConfig
from datascience.views import *

from datascience.api.autocomplete import *

from datascience.api.drf_views import *

app_name = DatascienceConfig.name


urlpatterns = [
    # API CALLS
    # RESTFUL ENDPOINTS
    # CATEGORY
    path(
        "w101_droprate/category",
        W101CategoryListView.as_view(),
        name="w101_droprate_category_list",
    ),
    path(
        "w101_droprate/category/autocomplete",
        W101CategoryAutocompleteView.as_view(),
        name="w101_droprate_category_autocomplete",
    ),
    path(
        "w101_droprate/category/<int:pk>",
        W101CategoryDetailView.as_view(),
        name="w101_droprate_category_detail",
    ),
    # ITEM
    path(
        "w101_droprate/item",
        W101ItemListView.as_view(),
        name="w101_droprate_item_source_list",
    ),
    path(
        "w101_droprate/item/autocomplete",
        W101ItemAutocompleteView.as_view(),
        name="w101_droprate_item_autocomplete",
    ),
    path(
        "w101_droprate/item/<int:pk>",
        W101ItemDetailView.as_view(),
        name="w101_droprate_item_source_detail",
    ),
    # ITEM SOURCE
    path(
        "w101_droprate/item_source",
        W101ItemSourceListView.as_view(),
        name="w101_droprate_item_source_list",
    ),
    path(
        "w101_droprate/item_source/autocomplete",
        W101ItemSourceAutocompleteView.as_view(),
        name="w101_droprate_item_source_category_autocomplete",
    ),
    path(
        "w101_droprate/item_source/<int:pk>",
        W101ItemSourceDetailView.as_view(),
        name="w101_droprate_item_source_detail",
    ),
    # DROP ATTEMPT
    path(
        "w101_droprate/drop_attempt",
        W101DropAttemptListView.as_view(),
        name="w101_droprate_drop_attempt_list",
    ),
    path(
        "w101_droprate/drop_attempt/<int:pk>",
        W101DropAttemptDetailView.as_view(),
        name="w101_droprate_drop_attempt_detail",
    ),
    path(
        "w101_droprate/drop_attempt/form",
        W101DropAttemptEndpointView.as_view(),
        name="w101_droprate_drop_attempt_form",
    ),
    # BROWSER PAGES
    path(
        "w101_droprate/",
        W101DropRateDashboardView.as_view(),
        name="w101_droprate_dashboard",
    ),
    path("moderation/", ModerationView.as_view(), name="moderation_home"),
    path("", IndexView.as_view(), name="projects"),
]
