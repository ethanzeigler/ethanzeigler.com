from django import urls


class DSProject:
    """
    A data science project
    """

    def __init__(
        self,
        name=None,
        short_name=None,
        description=None,
        title_img=None,
        slug=None,
        subtitle_description=None,
    ):
        self.name = name
        self.short_name = short_name
        self.description = description
        self.title_img = title_img
        self.slug = slug
        self.subtitle_description = subtitle_description


wizard101_critical = DSProject(
    name="Wizard101 Critical Rating to Percent",
    short_name="W101 Crit to %",
    slug="w101_droprate",
    title_img="images/datascience/w101crit/critical.png",
    subtitle_description=(
        "Wizard101's critical system formula isn't public. Using your "
        "donated data points, this project builds a percentile graph "
        "for each level, matching critical rating to percent."
    ),
    description=(
        "In the past, critical rating was a certain percentage and "
        "never changed. Upon the release of Polaris, direct "
        "coorelation died. The percentage fell each time you'd "
        "level. This formula from rating to percentage isn't known.\n"
        ""
        "Using data from you, we can fix this. This project will "
        "collect rating, level, and percent values to create a "
        "graph, finally showing these percents to the community."
    ),
)

projects = [wizard101_critical]
