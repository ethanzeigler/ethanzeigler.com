import json

import jsonschema
from jsonschema.exceptions import SchemaError, ValidationError


class JsonValidator:
    def __init__(self, schema: str, json_str: str) -> None:
        json_data = json.loads(json_str)
        validator = jsonschema.Draft7Validator(schema)
        try:
            validator.validate(json_data)
            self.valid = True
            self.validation_message = None
            self.data = json.loads(json_str)
        except (ValidationError, SchemaError) as err:
            self.valid = False
            breakpoint()
            self.validation_message = str(err)
            self.data = None
        

    
