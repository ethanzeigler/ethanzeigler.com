import logging
import uuid

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from django.contrib import admin
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.db.models.base import Model
from django.db.models.fields import BooleanField, UUIDField
from django.forms import ModelForm
from django.http import Http404
from django.utils import timezone
from django_currentuser.db.models.fields import CurrentUserField
from django_extensions.db.models import TimeStampedModel
from polymorphic.models import PolymorphicModel
from model_utils.managers import InheritanceManager

from users.models import CustomUser

logger = logging.getLogger(__name__)


class WorkflowState(models.TextChoices):
    NEW = "NEW", "new"
    APPROVED = "APR", "approved"
    DENIED = "DEN", "denied"
    DELETED = "DEL", "deleted"


class ModeratedModel(Model):
    objects = InheritanceManager()

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = CurrentUserField(on_delete=models.PROTECT, verbose_name="creator")
    workflow_state = models.CharField(
        max_length=5,
        null=False,
        blank=False,
        choices=WorkflowState.choices,
        default=WorkflowState.NEW,
    )
    approved_by = models.ForeignKey(
        CustomUser,
        on_delete=models.SET(None),  # TODO: Create sentinel user
        null=True,
        related_name="moderated_%(class)s_set",
    )
    external_id = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)

    class Meta:
        indexes = [
            models.Index(fields=["external_id"]),
            models.Index(fields=["workflow_state"]),
        ]

    def moderation_description(self):
        raise NotImplementedError

    def moderation_name(self):
        raise NotImplementedError

    def to_moderation_data(self):
        return {
            "uuid": self.external_id,
            "name": self.moderation_name(),
            "type": self.__class__.__name__,
            "description": self.moderation_description(),
            "username": self.created_by.username,
        }

    def approve(self, user: CustomUser) -> bool:
        if self.workflow_state != WorkflowState.NEW:
            logger.debug(f"cannot transition {self} {self.workflow_state} -> APPROVED. User {user}")
            return False
        # TODO: Message user
        self.approved_by = user
        self.workflow_state = WorkflowState.APPROVED
        self.save()
        logger.debug(f"transitioned {self} NEW -> APPROVED. User {user}")
        return True

    def deny(self, user: CustomUser, delete: bool = False) -> bool:
        """
        deny Denies the inputted event and sets the workflow to DELETED

        Args:
            user (CustomUser): [description]
            delete (bool, optional): [description]. Defaults to False.

        Returns:
            bool: [description]
        """
        if self.workflow_state in [WorkflowState.DENIED, WorkflowState.DELETED]:
            logger.info(f"cannot transition {self} {self.workflow_state} -> DENIED. User {user}")
            return False

        # TODO: Message user
        if delete:
            logger.debug(
                "Deleted %s completely. User %s",
                self,
                user,
            )
            self.delete()
        else:
            self.approved_by = None
            self.workflow_state = WorkflowState.DELETED
            self.save()
            logger.info(
                "transitioned %s %s -> %s. User %s",
                self.id,
                self.workflow_state,
                WorkflowState.DELETED,
                user,
            )
        return True


class CategoryAppliesTo(models.TextChoices):
    ANY = "AL", "Any"
    DROP = "DR", "Items"
    DROP_SOURCE = "DS", "Sources"


class W101Category(ModeratedModel):
    class Meta:
        verbose_name_plural = "W101 categories"
        # permissions = (("", ""))

    name = models.CharField(
        max_length=20,
        null=False,
        blank=False,
        unique=True,
        help_text="A general category like MOB, pack, mount, reagent, etc.",
    )
    applies_to = models.CharField(
        choices=CategoryAppliesTo.choices, max_length=2, null=False, blank=False
    )
    description = models.TextField(max_length=500, null=False, blank=True)

    def moderation_description(self):
        return self.description

    def moderation_name(self):
        return self.name

    def __str__(self) -> str:
        return self.name

    def to_moderation_data(self):
        return {**super().to_moderation_data(), "applies_to": self.applies_to}


class W101Item(ModeratedModel):

    name = models.CharField(max_length=45)
    description = models.TextField(
        max_length=500,
        null=False,
        blank=True,
        help_text="Please include a link to the Wizard101 Wiki. All new items are verified.",
    )
    category = models.ForeignKey(
        W101Category,
        help_text="If several categories apply, choose the most accurate",
        null=False,
        on_delete=models.PROTECT,
    )

    def moderation_description(self):
        return self.description

    def moderation_name(self):
        return self.name

    def to_moderation_data(self):
        return {**super().to_moderation_data(), "category": self.category.external_id}

    def __str__(self) -> str:
        return f"{self.name} [{self.category.name}]"


class W101ItemSource(ModeratedModel):

    name = models.CharField(max_length=35)
    description = models.TextField(
        max_length=500,
        null=False,
        blank=True,
        help_text="Please include a link to the Wizard101 Wiki. All new items are verified.",
    )
    category = models.ForeignKey(
        W101Category,
        null=False,
        on_delete=models.PROTECT,
        help_text="If several categories apply, choose the most accurate",
    )

    def moderation_description(self):
        return self.description

    def moderation_name(self):
        return self.name

    def to_moderation_data(self):
        return {**super().to_moderation_data(), "category": self.category.external_id}

    def __str__(self) -> str:
        return self.name


class W101DropAttempt(ModeratedModel):

    source = models.ForeignKey(
        W101ItemSource,
        null=False,
        on_delete=models.PROTECT,
        help_text="If the source does not exist, create it before continuing.",
    )
    opportunities_count = models.IntegerField(
        verbose_name="number of drop opportunities",
        help_text="The number of times the item could have been dropped over this attempt. You may combine several pack opens, dungeon runs, etc into one event if they are from the same source.",
        validators=[
            MinValueValidator(1, "Must have at least one opportunity"),
            MaxValueValidator(100, "Opportunities are limited to 100"),
        ],
    )
    drops = models.ManyToManyField(W101Item)

    def moderation_description(self):
        return f"""Opportunities: {self.opportunities_count}
            Source: {self.source.name}
            Drops: {self.drops}
        """

    def moderation_name(self):
        return f"{self.created_by.username}'s {self.source.name} run"

    def __str__(self) -> str:
        return f"{self.source.name} [n={self.opportunities_count}, by={self.created_by}, id={self.id}]"


class W101DropCount(Model):

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    item = models.ForeignKey(W101Item, on_delete=models.PROTECT)
    attempt = models.ForeignKey(W101DropAttempt, on_delete=models.CASCADE)
    count = models.IntegerField(
        verbose_name="number of items dropped",
        null=False,
        # help_text="Number of times this item dropped. Remember, 0 is valid and encouraged!",
        validators=[
            MinValueValidator(0, "Can't drop negative times!"),
            MaxValueValidator(100, "Drop counts are limited to 100"),
        ],
    )
    external_id = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)

    def __str__(self) -> str:
        return f"{self.item.name} [n={self.count}, attempt={self.attempt.id}, id={self.id}]"


admin.site.register(W101Category)
admin.site.register(W101ItemSource)
admin.site.register(W101DropAttempt)
admin.site.register(W101Item)
