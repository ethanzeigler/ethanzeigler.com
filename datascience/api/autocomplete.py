import logging

from datascience.models import (
    W101Category,
    W101Item,
    W101ItemSource,
    WorkflowState,
)

from django.db.models import Q
from django.http.response import (
    JsonResponse,
)
from django.views.generic.base import View

###########################################################
# Instance
logger = logging.getLogger(__name__)


class W101CategoryAutocompleteView(View):
    def get(self, request):
        qs = W101Category.objects.all()
        query = request.GET.get("q")
        include = request.GET.get("i")

        if query:
            # qs = qs.filter(name__icontains=query)
            search_query = Q(name__contains=query)
            if include and str(include).isnumeric():
                search_query = search_query | Q(external_id=include)
            qs = qs.filter(
                search_query, Q(workflow_state=WorkflowState.APPROVED) | Q(created_by=request.user)
            )

        mapping = {
            str(category[0]): category[1]
            for category in qs.values_list("external_id", "name")[:10]
        }

        return JsonResponse(mapping)


class W101ItemAutocompleteView(View):
    def get(self, request):
        qs = W101Item.objects.all()
        query = request.GET.get("q")
        include = request.GET.get("i")

        if query:
            # qs = qs.filter(name__icontains=query)
            search_query = Q(name__contains=query)
            if include and str(include).isnumeric():
                search_query = search_query | Q(id=include)
            qs = qs.filter(
                search_query, Q(workflow_state=WorkflowState.APPROVED) | Q(created_by=request.user)
            )

        mapping = {str(item[0]): item[1] for item in qs.values_list("id", "name")[:10]}

        return JsonResponse(mapping)


class W101ItemSourceAutocompleteView(View):
    def get(self, request):
        qs = W101ItemSource.objects.all()
        query = request.GET.get("q")
        include = request.GET.get("i")

        if query:
            # qs = qs.filter(name__icontains=query)
            search_query = Q(name__contains=query)
            if include and str(include).isnumeric():
                search_query = search_query | Q(id=include)
            qs = qs.filter(
                search_query, Q(workflow_state=WorkflowState.APPROVED) | Q(created_by=request.user)
            )

        mapping = {str(item[0]): item[1] for item in qs.values_list("id", "name")[:10]}
        return JsonResponse(mapping)
