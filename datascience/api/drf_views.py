import logging

from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from datascience.models import (
    W101Category,
    W101DropAttempt,
    W101Item,
    W101ItemSource,
)
from datascience.serializers import (
    W101CategorySerializer,
    W101DropAttemptSerializer,
    W101ItemSerializer,
    W101ItemSourceSerializer,
)

###########################################################
# Instance
logger = logging.getLogger(__name__)

class W101CategoryListView(ListCreateAPIView):
    queryset = W101Category.objects.all()
    serializer_class = W101CategorySerializer


class W101CategoryDetailView(RetrieveUpdateDestroyAPIView):
    queryset = W101Category.objects.all()
    serializer_class = W101CategorySerializer


class W101ItemListView(ListCreateAPIView):
    queryset = W101Item.objects.all()
    serializer_class = W101ItemSerializer


class W101ItemDetailView(RetrieveUpdateDestroyAPIView):
    queryset = W101Item.objects.all()
    serializer_class = W101ItemSerializer


class W101ItemSourceListView(ListCreateAPIView):
    queryset = W101ItemSource.objects.all()
    serializer_class = W101ItemSourceSerializer


class W101ItemSourceDetailView(RetrieveUpdateDestroyAPIView):
    queryset = W101ItemSource.objects.all()
    serializer_class = W101ItemSourceSerializer


class W101DropAttemptListView(ListCreateAPIView):
    queryset = W101DropAttempt.objects.all()
    serializer_class = W101DropAttemptSerializer


class W101DropAttemptDetailView(RetrieveUpdateDestroyAPIView):
    queryset = W101DropAttempt.objects.all()
    serializer_class = W101DropAttemptSerializer