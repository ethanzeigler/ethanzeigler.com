def _critical_line_graph_data(**kwargs):
    if kwargs.get("fake") == "1":
        levels, ratings, percents = zip(*[(i, i, 90) for i in range(1, 130)])
        return {
            "status": "200",
            "msg": "OK",
            "data": {"levels": levels, "ratings": ratings, "percents": percents},
        }

    # FIXME: this is dangerous
    data = W101CritDP.objects.values_list("level", "rating", "percent")
    levels, ratings, percents = zip(*list(data))
    return {
        "status": "200",
        "msg": "OK",
        "data": {"levels": levels, "ratings": ratings, "percents": percents},
    }


def get_data(request, **kwargs):
    """Gets the data as a map (to be unmarshalled into json)

    Args:
        request (Str): request type
    """
    print("incoming params:", kwargs)
    if kwargs["ds"] == "lines":
        return _critical_line_graph_data(**kwargs)
    else:
        return {"status": "400", "msg": f"unknown dataset { kwargs['ds'] }"}
