import CCF from "../../components/datascience/ComposedCategoryForm"

export default {
    title: 'Form/CategoryForm',
    component: CCF,
    // More on argTypes: https://storybook.js.org/docs/vue/api/argtypes
    argTypes: {
      backgroundColor: { control: 'color' },
      onClick: {},
      size: {
        control: { type: 'select' },
        options: ['small', 'medium', 'large'],
      },
    },
  };
  