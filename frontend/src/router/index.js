import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: "/test",
    name: "Test",
    component: () => import( /* webpackChunkName: "about" */ '../views/Test.vue')
  },
  {
    path: "/datascience/w101_droprate",
    name: "ds_w101_dr",
    component: () => import("@/views/datascience/W101Droprate.vue")
  },
  {
    path: '/datascience',
    name: "Datascience",
    component: () => import("@/views/datascience/Home.vue")
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

export default router
