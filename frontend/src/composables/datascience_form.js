import {
  ref,
  reactive,
  watch,
} from 'vue';
import useVuelidate from '@vuelidate/core';
import {
  sendModerationAction as sendModerationActionComm,
  CODE_SUCCESS,
  CODE_ILLEGAL
} from '@/utils/coms';
import {
  showAlert
} from '../utils/alerts';

const moderationActionPastTenses = {
  "approve": "approved",
  "deny": "denied"
}

const defaultModeConfig = {
  "create": {
    "title": "Create New ???",
    "buttons": {
      "ok": "Create",
      "deny": null,
    }
  },
  "review": {
    "title": "Review ???",
    "buttons": {
      "ok": "Approve",
      "deny": "Deny",
    }
  },
}


export const withProps = () => {
  return {
    model: {
      type: Object,
      optional: true
    },
    disable: {
      type: Boolean,
      optional: true,
    },
    mode: {
      type: String,
      optional: false,
    },
  }
}

export const withEmits = () => {
  [
    "update:model",
    "update:validation", 
    "submit:created",
    "submit:failedValidation",
    "submit:updated",
    "submit:approved",
    "submit:denied",
    "submit:moderationError", // event data: string reason
    "submit:canceled",
    "modal:open",
    "modal:close",
  ]
}

export function useDatascienceForm({
  props,
  setupContext,
  validationRules,
  //crudEndpoint,
}) {
  let modelRef = ref(props.model)
  const state = reactive({
    // copy of the model that mirrors changes from the outside but not vice
    // versa unless some event causes it.
    modelInternalCopy: Object.assign({}, modelRef.value),
    // internal control of whether the form is disabled during events such as
    // submittal. ANDed with prop.
    disableInternal: false
  })

  // vuelidate's place to hold validation errors from the backend
  const $externalResults = ref({})

  // validations from the using form
  const v = useVuelidate(validationRules, state, {
    $externalResults
  })

  // changes in the prop model always mirror to the local copy
  watch(modelRef, (currentModelRef, previousModelRef) => {
    if (currentModelRef !== previousModelRef) {
      state.modelInternalCopy = currentModelRef
    }
  })

  async function sendCreateAction() {
    
  }

  // handles moderation actions and the results of the request
  async function sendModerationAction(action) {
    switch (action) {
      case "approve":
      case "deny":
        sendModerationActionComm(action, state.modelInternalCopy.uuid).then(resp => {
          switch (resp.status) {
            case CODE_SUCCESS:
              showAlert({
                message: `${moderationActionPastTenses[action]} ${state.modelInternalCopy.name}`,
                timeout: 5,
                level: "success"
              });
              setupContext.emit(`submit:${moderationActionPastTenses[action]}`)
              break;
            case CODE_ILLEGAL:
              showAlert({
                message: `Can't ${action}: ${resp.data.result}`
              });
              setupContext.emit("submit:moderationError", resp.data.result)
          }
        }).catch(err => {
          console.err(`Moderation action failed: ${err}`);
        });
        break;
      default:
        showAlert({
          message: `Unrecognized moderation action \`${action}\``,
          timeout: 5,
          level: "success"
        });
    }
  }

  async function sendCRUDAction(action) {

  }

  return {
    state,
    v$: v,
    sendModerationAction
  }

}
