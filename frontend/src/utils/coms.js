// const { getCookie } = require("../global_lib/misc");
import { showAlert } from "./alerts";
const axios = require("axios");
const ajaxHeaders = {
    headers: {
        "content-type": "application/json",
        'X-CSRFToken': "placeholder",
        'X-IS-AJAX': true
    }
};

export const CODE_SUCCESS = 200
export const CODE_ILLEGAL = 422
export const CODE_UNAUTHORIZED = 403
export const CODE_NET_ERR = 0


async function myCreateModel({ input, inputTypeName, endpoint, sanitizer }) {
    // simple data cleanup. Weird stuff might get in there.
    const sanitized = sanitizer(input);
    let res = null;
    try {
        res = await axios.post(endpoint, sanitized, ajaxHeaders);
        return res;
    } catch (err) {
        // validation error
        genericFailureAlertFor(err.response.status);
        if (err.response.status == 400) {
            return err.response;
        }
        // something else
        showAlert({
            message: `Couldn't submit ${inputTypeName}. Something went wrong. Sorry!`,
            dismissable: true
        });
        console.info(err);
        return err;
    }
}

export async function myAutocompleteModel({ endpoint, inputTypeName, transformer, search, queryParams = {} }) {
    let requestURI = `${endpoint}?q=${encodeURIComponent(search)}`;
    Object.entries(queryParams).forEach(([k, v]) => {
        requestURI += `&${encodeURIComponent(k)}=${encodeURIComponent(v)}`
    });
    let res = null;
    try {
        res = await axios.get(requestURI, ajaxHeaders);
        const transformed = transformer(res.data);
        console.debug(`Transformed ${inputTypeName} autocompletes to`, transformed);
        return transformed;
    } catch (err) {
        // validation error
        genericFailureAlertFor(err.response.status);
        throw new Error("Bad Response");
    }
}

export function sendModerationAction(action, uuid) {
    console.debug("Requesting moderation action", action, "on", uuid);
    return axios.post("/datascience/moderation/", {
        "action": action,
        "uuid": uuid,
    }, ajaxHeaders).then(res => {
        let data = res.data;
        if (data.status !== 200) {
            genericFailureAlertFor(data.status);
        }
        return res;
    }).catch(err => {
        let status_code = err.response ? err.response.status : 0
        if (status_code == 422) {
            return err.response;
        }
        genericFailureAlertFor(err.response.status);
        return err.response;
    });
}

function genericFailureAlertFor(code) {
    switch(code) {
        case CODE_UNAUTHORIZED:
            showAlert({
                message: `You don't have permission to do this. Are you logged in?`,
                level: "error",
                dismissable: true
            });
            break;
        case CODE_ILLEGAL:
            showAlert({
                message: `The action you requested isn't possible. Did something change?`,
                level: "warning",
                dismissable: true
            });
            break;
        case CODE_NET_ERR:
            showAlert({
                message: `A request to the backend failed. Please try again.`,
                level: "warning",
                dismissable: true
            });
            break;
        default:
            showAlert({
                message: `Unhandled: generic alert for ${code}`,
                level: "warning",
                dismissable: true
            });
    }
}


export async function getModerationQueue() {
    let res = null;
    try {
        res = await axios.get("/datascience/moderation/?id=all");
        let data = res.data;
        if (data.status !== CODE_SUCCESS) {
            throw "invalid status";
        }
        console.debug("Retrieved moderation data:", data.moderated);
        return data.moderated;
    } catch {
        showAlert({
            message: `Couldn't get moderation queue: Status ${res.status}.`,
            dismissable: true
        })
        return [];
    }
}

///////////////////////////////////////////////////
//                  RESTFUL
///////////////////////////////////////////////////

/////////////
// Category
/////////////
export async function createCategory(input) {
    return await myCreateModel({
        input: input,
        inputTypeName: "category",
        endpoint: "/datascience/w101_droprate/category",
        sanitizer: x => {
            return {
                name: x.name,
                applies_to: x.applies_to,
                description: x.description
            }
        }
    });
}

export async function getAutocompleteCategories(search, include = null, restriction = null) {
    const params = {};
    if (include) {
        params.i = include;
    }
    if (restriction) {
        params.r = restriction;
    }
    return await myAutocompleteModel({
        search: search,
        queryParams: params,
        inputTypeName: "category",
        endpoint: "/datascience/w101_droprate/category/autocomplete",
        transformer: x => Object.entries(x).map(([k, v]) => {
            return {
                uuid: k,
                label: v
            }
        })
    })
}


////////////////
// Itemsource
////////////////
export async function createItemSource(input) {
    return await myCreateModel({
        input: input,
        inputTypeName: "item source",
        endpoint: "/datascience/w101_droprate/item_source",
        sanitizer: x => {
            return {
                name: x.name,
                category: x.category,
                description: x.description
            }
        }
    });
}

export async function getAutocompleteItemSources(search, include = null) {
    const params = {};
    if (include) {
        params.i = include;
    }
    return await myAutocompleteModel({
        search: search,
        queryParams: params,
        inputTypeName: "item source",
        endpoint: "/datascience/w101_droprate/item_source/autocomplete",
        transformer: x => Object.entries(x).map(([k, v]) => {
            return {
                uuid: k,
                label: v["name"],
                category: v["category"]
            }
        })
    });
}

/////////////////
// Item
/////////////////
export async function createItem(input) {
    return await myCreateModel({
        input: input,
        inputTypeName: "item",
        endpoint: "/datascience/w101_droprate/item",
        sanitizer: x => {
            return {
                name: x.name,
                category: x.category,
                description: x.description
            }
        }
    });
}

export async function getAutocompleteItems(search, include = null) {
    return await myAutocompleteModel({
        search: search,
        include: include,
        inputTypeName: "item",
        endpoint: "/datascience/w101_droprate/item/autocomplete",
        transformer: x => Object.entries(x).map(([k, v]) => {
            return {
                uuid: k,
                label: v["name"],
                category: v["category"]
            }
        })
    })
}