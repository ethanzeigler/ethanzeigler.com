// from https://www.w3schools.com/js/js_cookies.asp
export function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
// from https://www.w3schools.com/js/js_cookies.asp
export function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

export function deleteCookie(cname) {
    document.cookie = `${cname}=; expires=Thu, 01 Jan 1970 00:00:00 UTC;`;
}

/**
 * Given a namespace and file, reports if the module was already loaded somewhere else
 * @param {namespace} namespace 
 * @param {String} jsFile 
 */
export function isFirstLoad(namespace, jsFile) {
    const isFirst = namespace.firstLoad === undefined;
    namespace.firstLoad = false;

    if (!isFirst) {
        console.log(`Warning: module ${namespace} is loaded twice within ${jsFile}`);
    }

    return isFirst;
}

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions#escaping
export function escapeRegExp(string) {
    return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
}
  

export function safeLinkify(text, allowed_domains) {
    const options = {
        defaultProtocol: 'https',
        validate: (value, type) => {
            if (type !== 'url') {
                return false;
            }

            return allowed_domains.some(domain => {
                const escaped_domain = escapeRegExp(domain);
                const regex = new RegExp(`^(https?:\\/\\/)?${escaped_domain}`);
                if (regex.test(value)) {
                    return true;
                }
            });
        }
    };
    return text.linkify(options);
}

export function truncateText(text, max, ending) {
    if (max == null) {
        max = 100;
    }
    if (ending == null) {
        ending = '...';
    }
    if (text.length > max) {
        return text.substring(0, max - ending.length) + ending;
    } else {
        return text;
    }
}