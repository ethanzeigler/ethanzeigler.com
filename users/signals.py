from os import getgroups

from django.contrib.auth.models import Group
from django.db.models.signals import post_save
from django.dispatch import receiver

from users.models import CustomUser


@receiver(post_save, sender=CustomUser)
def set_initial_permission_group(sender, instance, created, **kwargs):
    if created:
        default_group = Group.objects.get(name="User")
        instance.groups.add(default_group)
