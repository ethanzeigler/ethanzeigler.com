import json
from django.http.response import JsonResponse
from django.views.generic import TemplateView
from django.conf import settings


class HomePageView(TemplateView):
    template_name = "pages/home.html"


class AboutPageView(TemplateView):
    template_name = "pages/about.html"


class AppsPageView(TemplateView):
    template_name = "pages/apps.html"


class DRFIndexPageView(TemplateView):
    template_name = "pages/drf_index.html"


class ResumePageView(TemplateView):
    template_name = "pages/resume.html"

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            # Expecting json object containing {'codeword':'...'}
            # Validated against codeword in settings
            if not request.is_ajax():
                data["status"] = "content-type not json"
            else:
                form = json.loads(request.body)
                if form["codeword"].lower() == settings.RESUME_CODEWORD.lower():
                    data["status"] = "OK"
                    data["redirect"] = str(settings.FULL_RESUME_URL)
                else:
                    data["status"] = "unauthorized"
        except:
            data["status"] = "malformatted request"
        finally:
            return JsonResponse(data=data)

################################################################################
################################################################################

####################
# Development only
class TestPageView(TemplateView):
    template_name = "pages/test.html"

class BootstrapPageView(TemplateView):
    template_name = "pages/bootstrap.html"

