from django.urls import path
from django.conf import settings

from .views import AppsPageView, BootstrapPageView, DRFIndexPageView, HomePageView, AboutPageView, ResumePageView, TestPageView

urlpatterns = [
    path("", HomePageView.as_view(), name="home"),
    path("about/", AboutPageView.as_view(), name="about"),
    path("apps/", AppsPageView.as_view(), name="apps_index"),
    path("resume/", ResumePageView.as_view(), name="resume"),
]

if settings.DEBUG:
    development_patterns = [
        path("test/", TestPageView.as_view(), name="test"),
        path("bootstrap/", BootstrapPageView.as_view(), name="bootstrap_index"),
        path("drf_index/", DRFIndexPageView.as_view(), name="drf_index"),
    ]
    urlpatterns.extend(development_patterns)

